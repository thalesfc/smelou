from app import app, facebook, lm, db
from flask import render_template, flash, redirect, session, url_for, g, make_response, request, Response
from flask.ext.login import login_user, current_user, logout_user, login_required
from models import User, View
from history import process_data
from flask.ext.wtf import Form
from wtforms import BooleanField, SelectMultipleField
from wtforms import widgets
from config import EMAIL_SAVE_FOLDER


app.config.from_object('config')


@app.route('/')
@app.route('/index')
def index():
    if g.user is not None and g.user.is_authenticated():
        views = g.user.views_to_select()
        options = [(v.object.id, v.object.title) for v in views]
        checked = [v.object.id for v in views if v.show]

        # subclass form
        class F(Form):
            selector = SelectMultipleField(
                option_widget=widgets.CheckboxInput(),
                widget=widgets.ListWidget(prefix_label=False),
                choices=options,
                default=checked)

        # data = [(v.object.title, v.object.id) for v in views]
        #
        # class F(Form):
        #     pass
        #
        # for v in views:
        #     if v.show == True:
        #         setattr(F, str(v.object.id), BooleanField(v.object.title, default=True))
        #
        #     else:
        #         setattr(F, str(v.object.id), BooleanField(v.object.title, default=False))

        form = F()
        resp = make_response(render_template('logged.html', user=g.user, form=form))

        resp.set_cookie('smelou_user_id', g.user.id)

        return resp

    else:
        return render_template('login.html')


@app.route("/save_email", methods=['POST'])
def save_email():
    email = request.form['email'].strip()

    if email:
        file = open(EMAIL_SAVE_FOLDER, 'w')
        file.write('%s\n' % email)
        file.close()

        return redirect(url_for('ajax_ok'))


@app.route("/ajax_ok")
def ajax_ok():
    return Response('ok')


@app.route("/home")
def home():
    return render_template('home.html')


@app.route("/login")
def facebook_login():
    return facebook.authorize(
        callback=url_for('facebook_authorized', _external=True)
    )


@app.route("/profile/<nickname>")
def profile(nickname):
    user = User.query.filter_by(nickname=nickname).first()

    if user is None:
        flash('No user under nickname %s were found! :(' % nickname, 'error')

        return redirect(url_for('index'))

    return render_template('profile.html', user=user)


@app.route("/update_profile", methods=['POST'])
def update_profile():
    for k, v in request.form.items():
        if k != "csrf_token":
            pageid = int(k)
            view = View.query.filter_by(user_id=g.user.id, page_id=pageid).first()

            if v == "y":
                view.show = True

            else:
                view.show = False

            db.session.add(view)

    flash('Seu perfil foi atualizado')
    db.session.commit()

    return str(request.form.items())
    # return redirect(url_for('index'))


@app.route("/submit/<id>", methods=['POST'])
def submit_data(id):
    if not g.user.is_authenticated():
        return 'You must login to push data', 401

    elif g.user.id == id:
        if g.user.has_extension is False:
            g.user.has_extension = True

            # user has extension runnig up
            db.session.add(g.user)
            db.session.commit()

        # process data
        json = request.get_json(force=True)

        process_data(json)

        code = 200

        if g.user.has_profile is False:
            code = 202
            g.user.has_profile = True  # user now has profile

            db.session.add(g.user)
            db.session.commit()

        return 'Data submitted', code

    else:
        return 'You are trying to push data for another user. Shame on you! :(', 403


@lm.user_loader
def load_user(id):
    return User.query.get(id)


@app.route("/facebook_authorized")
@facebook.authorized_handler
def facebook_authorized(resp):
    if resp is None or 'access_token' not in resp:
        flash('Error ao logar com seu facebook: %s' % resp, 'error')

    else:
        # facebook api reference
        # https://developers.facebook.com/docs/graph-api/reference/v2.1/user/picture
        session['facebook_token'] = (resp['access_token'], '')
        data = facebook.get('/me').data
        picture = facebook.get('me/picture', data={'redirect': False, 'type': 'large'}).data
        # seeking user
        user = load_user(data['id'])

        if user is None:
            nickname = data['name'].replace(' ', '.').lower()
            user = User(nickname=nickname, id=data['id'])

            db.session.add(user)

        # updating user
        user.name = data['name']
        user.email = data['email']
        user.picture = picture['data']['url']

        db.session.commit()
        login_user(user, remember=True)

    return redirect(url_for('index'))


@facebook.tokengetter
def get_facebook_token():
    return session.get('facebook_token')


@app.before_request
def before_request():
    g.user = current_user


@app.route('/logout')
def logout():
    logout_user()
    if 'facebook_token' in session:
        session.pop('facebook_token')

    return redirect(url_for('index'))
