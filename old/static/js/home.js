$(function() {
    $('#signup-form').submit(function() {
        var $form = $(this),
            email = $('#email-input').val();

        $('#submit-response').children().addClass('hidden');

        if (!email.length) {
            $('#empty-email-response').removeClass('hidden');

        } else {
            $.post($form.attr('action'), {'email': email})
            .done(function(data, state, xhr) {
                if (xhr.responseText == 'ok') {
                    $('#success-request-response').removeClass('hidden');
                    $('#email-input').val('');
                }
            })
            .fail(function() {
                $('#invalid-request-response').removeClass('hidden');
            });
        }

        return false;
    });
});
