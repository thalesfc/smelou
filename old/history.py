from app import models, db
from flask.ext.login import login_required
from flask import g
from helpers.thumbnail import clean_url, fetch_url, find_thumbnail_image
from helpers.classifier import is_product_page
from sys import stderr
from config import URL_SIZE
import BeautifulSoup


@login_required
def process_data(json):
    count = 1

    for jpage in json:
        # check if page exists, otherwhise create it
        url = clean_url(jpage['url'])[:URL_SIZE]

        print '# (%d) %s' % (count, url)

        page = models.Page.query.filter_by(url=url).first()

        if page is None:
            content_type, content = fetch_url(jpage['url'])

            if is_content_invalid(content_type, content):
                print >> stderr, 'Error: Invalid page at url %s' % url

                page = models.Page(url=url, title=jpage['title'], is_product=False)

            else:
                soup = BeautifulSoup.BeautifulSoup(content)
                is_product = is_product_page(content_type, content, url, soup)
                picture = None

                if is_product:
                    picture, _ = find_thumbnail_image(content_type, content, url, soup)

                page = models.Page(url=url, title=jpage['title'], picture=picture, is_product=is_product)

            db.session.add(page)

        # now page exist, check if this user already have it
        view = models.View.query.filter_by(page_id=page.id, user_id=g.user.id).first()

        if view is None:
            print 'NO VIEW'

            view = models.View(page_id=page.id, user_id=g.user.id)

            db.session.add(view)

        else:
            print 'yeas view!!!'
            # TODO maybe update it

        count += 1

        if count % 100 == 0:
            db.session.commit()

    db.session.commit()
    print 75 * '#'


def is_content_invalid(content_type, content):
    if content is None or 'html' not in content_type:
        return True

    return False
