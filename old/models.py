from app import db
from sqlalchemy import UniqueConstraint
from config import URL_SIZE


class User(db.Model):
    id = db.Column(db.String(32), primary_key=True)
    nickname = db.Column(db.String(120), index=True, unique=True)
    name = db.Column(db.String(200), index=True)
    email = db.Column(db.String(120), unique=True)
    picture = db.Column(db.Text)
    pages = db.relationship('View', backref='viewer', lazy='dynamic')
    has_extension = db.Column(db.Boolean, default=False)
    has_profile = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<User %s %r>' % (self.name, self.nickname)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    # return the list of public profiled pages
    def get_pages(self):
        returned = []

        for p in self.pages:
            if p.show:
                returned.append(p.object)

        return returned

    # return a list of view already marked for public show
    # or those views of products not marked yet
    def views_to_select(self):
        returned = []

        for p in self.pages:
            if p.show is True:
                returned.append(p)

            elif p.show is None and p.object.is_product:
                returned.append(p)

        return returned


class View(db.Model):
    __tablename__ = 'view'

    id = db.Column(db.BigInteger, primary_key=True)
    user_id = db.Column(db.String(32), db.ForeignKey('user.id'), index=True)
    page_id = db.Column(db.BigInteger, db.ForeignKey('page.id'), index=True)
    show = db.Column(db.Boolean, default=None)


class Page(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    url = db.Column(db.String(URL_SIZE), index=True, unique=True)
    title = db.Column(db.Text)
    picture = db.Column(db.Text)
    is_product = db.Column(db.Boolean, index=True)
    users = db.relationship('View', backref='object', lazy='dynamic')
