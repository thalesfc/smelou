# creating the app
from flask import Flask

app = Flask(__name__)
app.config.from_object('config')

# facebook login
from config import FACEBOOK_CONFIG, DEBUG
from flask_oauth import OAuth

oauth = OAuth()
facebook = oauth.remote_app('facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key=FACEBOOK_CONFIG['app_id'],
    consumer_secret=FACEBOOK_CONFIG['app_secret'],
    request_token_params={'scope': FACEBOOK_CONFIG['scope']}
)

# database
from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy(app)

# flask login
from flask.ext.login import LoginManager

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'index'

# loading views
from app import views, models
