# reddit thumbnail's code
# sourced from https://github.com/reddit/reddit/blob/master/r2/r2/lib/media.py
# commit number: 11ac18a77a6473d930ef3aa1174128e467c88832
# commit title: OAuth2: Fix app icon uploading

import urllib2
import cStringIO
import gzip
import BeautifulSoup
import urlparse
from PIL import ImageFile
from flask import request
from sys import stderr
import socket


def _fetch_image_size(url, referer):
    """Return the size of an image by URL downloading as little as possible."""

    request = _initialize_request(url, referer)

    if not request:
        return None

    parser = ImageFile.Parser()
    response = None

    try:
        response = urllib2.urlopen(request)

        while True:
            chunk = response.read(1024)

            if not chunk:
                break

            try:
                parser.feed(chunk)
            except ValueError:
                if response:
                    response.close()

                return None, None

            if parser.image:
                return parser.image.size

    except (urllib2.URLError, socket.timeout, socket.error):
        print >> stderr, 'Error while fetching url %s' % url

        if response:
            response.close()
        return None, None


def clean_url(url):
    '''url quotes unicode data out of urls'''

    url = url.encode('utf8')
    url = ''.join(urllib2.quote(c) if ord(c) >= 127 else c for c in url)

    return url


def _initialize_request(url, referer, gzip=False):
    url = clean_url(url)

    if not url.startswith(('http://', 'https://')):
        return

    req = urllib2.Request(url)

    if gzip:
        req.add_header('Accept-Encoding', 'gzip')

    if 'User-Agent' in request.headers:
        req.add_header('User-Agent', request.headers.get('User-Agent'))

    if referer:
        req.add_header('Referer', referer)

    return req


def fetch_url(url, referer=None):
    request = _initialize_request(url, referer=referer, gzip=True)

    if not request:
        return None, None

    response = None

    try:
        response = urllib2.urlopen(request)

    except (urllib2.URLError, socket.error):
        print >> stderr, 'Timout while fetching url %s' % url

        if response:
            response.close()

        return None, None

    response_data = None

    try:
        response_data = response.read()

    except (socket.error):
        print >> stderr, 'Error while reading url %s' % url

        if response:
            response.close()

        return None, None

    content_encoding = response.info().get('Content-Encoding')

    if content_encoding and content_encoding.lower() in ['gzip', 'x-gzip']:
        buf = cStringIO.StringIO(response_data)
        f = gzip.GzipFile(fileobj=buf)
        response_data = f.read()

    return response.headers.get('Content-Type'), response_data


def _extract_image_urls(url, soup):
    for img in soup.findAll('img', src=True):
        yield urlparse.urljoin(url, img['src'])


def find_thumbnail_image(content_type, content, url, soup=None):
    if content_type is None or content is None:
        return None, None

    # if it's an image. it's pretty easy to guess what we should thumbnail.
    if content_type and 'image' in content_type and content:
        return url, None

    if content_type and 'html' in content_type and content:
        if not soup:
            soup = BeautifulSoup.BeautifulSoup(content)

    else:
        return None, None

    # allow the content author to specify the thumbnail:
    # <meta property="og:image" content="http://...">
    og_image = (soup.find('meta', property='og:image') or
                soup.find('meta', attrs={'name': 'og:image'}))

    if og_image and og_image['content']:
        return og_image['content'], 'metatag'

    # <link rel="image_src" href="http://...">
    thumbnail_spec = soup.find('link', rel='image_src')

    if thumbnail_spec and thumbnail_spec['href']:
        return thumbnail_spec['href'], 'link_image_src'

    # ok, we have no guidance from the author. look for the largest
    # image on the page with a few caveats. (see below)
    max_area = 0
    max_url = None

    for image_url in _extract_image_urls(url, soup):
        print '#', image_url

        size = _fetch_image_size(image_url, referer=url)

        if not size or not size[0] or not size[1]:
            continue

        area = size[0] * size[1]

        # ignore little images
        if area < 5000:
            print 'ignore little %s' % image_url

            continue

        # ignore excessively long/wide images
        if max(size) / min(size) > 1.5:
            print 'ignore dimensions %s' % image_url

            continue

        # penalize images with "sprite" in their name
        if 'sprite' in image_url.lower():
            print 'penalizing sprite %s' % image_url

            area /= 10

        if area > max_area:
            max_area = area
            max_url = image_url

    return max_url, 'max_area'

if __name__ == '__main__':
    # config variables
    class Temp():
        def __init__(self):
            self.headers = {}

    request = Temp()

    socket.setdefaulttimeout(2)  # timeout in seconds

    url = 'http://www.netshoes.com.br/produto/tenis-nike-reax-8-tr-004-5297-989'
    url = 'http://www.lojadobebe.com.br/'
    url = 'http://www.portalinformatica.com.br/mouse-logitech-m165-wireless-usb-preto.html'
    # url = 'http://localhost:5000/index' # timeout example

    # scraping
    url = clean_url(url)

    print "url:", url

    content_type, content = fetch_url(url)

    print "Type: %s - Content:\n%s" % (content_type, content)

    thumbnail, method = find_thumbnail_image(content_type, content)

    print "Thumbnail:", thumbnail
    print "Method:", method
