import BeautifulSoup


def is_product_page(content_type, content, url, soup):
    if not content or not content_type:
        return False

    if 'html' not in content_type:
        return False

    if not soup:
        return False

    page_text = soup.getText()
    count_dolar = page_text.count('$')

    print '$$$: %s -> %s' % (url, count_dolar)

    if count_dolar > 50:
        return True

    else:
        return False
