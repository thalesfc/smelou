# -*- coding: utf-8 -*-

from app import db, models
from urllib import urlopen


items = models.Item.query.all()


for i in items:
    code = urlopen(i.image).getcode()
    if code != 200:
        print code, i.image
        i.image = None
        db.session.add(i)
        db.session.commit()
