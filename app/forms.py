# -*- coding: utf-8 -*-
from flask.ext.wtf import Form
import wtforms
from wtforms.validators import URL


class EditItemForm(Form):
    id = wtforms.HiddenField()
    name = wtforms.StringField()
    normalized = wtforms.StringField()
    category = wtforms.StringField()
    image = wtforms.StringField(validators=[URL()])
    price = wtforms.StringField()
    score = wtforms.RadioField(
        choices=[('0.1', 'Baixo'), ('0.3', 'Medio'),
                 ('0.6', 'Alto')]
    )
    gender = wtforms.RadioField(
        choices=[('M', 'Homem'), ('W', 'Mulher'),
                 ('B', 'Ambos')]
    )
    store = wtforms.RadioField(
        choices=[('sephora', 'sephora'), ('imaginarium', 'imaginarium'),
                 ('vivara', 'vivara'), ('hering', 'hering')]
    )


NO_ANSWER_LABEL = u"Não quero responder"
BUYING_OPTIONS = ['Conforto', 'Marca', u'Tendência',
                  'Algo que traduza meu estilo']
HOBBY = ['Ler um livro', 'Sair com os amigos',
         'Ir ao cinema', 'Viajar']
COLORS = ['Preto', 'Cores neutras (bege, cinza, branco)',
          'Tons vibrantes', 'Estampas / Mistura de cores']


class QuestionaryForm(Form):
    birthday = wtforms.DateField('birthday', format='%d/%m/%Y')
    party = wtforms.TextField('party', default='Festa de 15 anos')
    shoes_size = wtforms.SelectField(
        'shoes_size',
        default=0,
        choices=[('', NO_ANSWER_LABEL)] +
        [(str(e), str(e)) for e in range(33, 46)]
        )
    shirt_size = wtforms.SelectField(
        'shirt_size',
        default=0,
        choices=[('', NO_ANSWER_LABEL), ('PP', 'PP'), ('P', 'P'),
                 ('M', 'M'), ('G', 'G')]
        )
    pants_size = wtforms.SelectField(
        'pants_size',
        default=0,
        choices=[('', NO_ANSWER_LABEL)] +
        [(str(e), str(e)) for e in range(34, 55, 2)]
        )
    buying = wtforms.RadioField(
        'buying',
        default='',
        choices=[('', NO_ANSWER_LABEL)] +
        [(e, e) for e in BUYING_OPTIONS]
        )
    hobby = wtforms.RadioField(
        'hobby',
        default='',
        choices=[('', NO_ANSWER_LABEL)] +
        [(e, e) for e in HOBBY]
        )
    color = wtforms.RadioField(
        'color',
        default='',
        choices=[('', NO_ANSWER_LABEL)] +
        [(e, e) for e in COLORS]
        )
