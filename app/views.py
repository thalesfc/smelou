# -*- coding: utf-8 -*-

# Python Core.
from json import dumps, loads


# Python Libs.
from flask import g, session
from flask import abort, render_template, request, Response, url_for, redirect
from flask import flash
from flask.ext.login import login_user, logout_user
from flask.ext.login import current_user, login_required
from slugify import Slugify
from sqlalchemy.sql.expression import func

# Smelou.
from app import app, facebook, login_manager, db
from models import User, Item, Opinion, Questionary
from forms import QuestionaryForm, EditItemForm


_MAX = 5
_SLUGY = Slugify(to_lower=True, separator='_')


def _get_items(page=1):
    if page >= 1:
        ops = Opinion.query.filter(
            Opinion.user_id == g.user.id,  # noqa
            Opinion.item_id != None
        )
        qry = Item.query.filter(Item.id.notin_([x.item_id for x in ops]))
        qry = qry.order_by(func.random()).paginate(1, 5, False).items

        return reversed(qry)


####################################
#       landing page
####################################
@app.route('/en/<page>')
def en_route(page):
    if page == 'teenagers':
        return render_template('teenagers.html', campaings=True)

    else:
        return render_template('en_landing.html')


@app.route('/')
@app.route('/login')
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('home'))

    else:
        return render_template('landing.html')


@app.route('/giving')
def giving():
    return render_template('giving.html', campaings=True)


@app.route('/birthdays')
def birthdays():
    return render_template('birthdays.html', campaings=True)


@app.route('/privacy_policy')
def privacy_policy():
    return render_template('privacy_policy.htm')


####################################
#       login manager
####################################
@login_manager.user_loader
def load_user(id):
    return User.query.get(id)


@app.before_request
def before_request():
    g.user = current_user


@app.route('/logout')
def logout():
    logout_user()

    if 'facebook_token' in session:
        session.pop('facebook_token')

    return redirect(url_for('login'))


@app.route('/mail')
def redirect_mail():
    return redirect('https://www.zoho.com/mail/login.html')


@app.route("/facebook_login")
def facebook_login():
    url = url_for('facebook_authorized', _external=True)

    return facebook.authorize(callback=url)


@facebook.tokengetter
def get_facebook_token():
    return session.get('facebook_token')


@app.route('/facebook_authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    if not resp or 'access_token' not in resp:
        flash('Error ao logar com seu facebook: %s' % resp, 'error')

        return redirect(url_for('login'))

    else:
        session['facebook_token'] = (resp['access_token'], '')
        fb_data = facebook.get('/me').data

        # id
        fb_id = fb_data['id']
        user = User.query.get(fb_id)

        if user is None:
            user = User(
                id=fb_id,
                nickname=User.generate_nickname(fb_data['name'])
            )

        # email
        user.email = fb_data['email']

        # picture
        fb_picture = facebook.get(
            'me/picture',
            data={'redirect': False, 'type': 'large'}
        ).data

        user.picture = fb_picture['data']['url']

        # birthday
        user.birthday = fb_data.get('birthday', None)

        # name
        user.name = fb_data.get('name', '')

        # gender
        user.gender = fb_data.get('gender', '')

        # first name
        user.first_name = fb_data.get('first_name', '')

        # link to fb
        user.fb_link = fb_data['link']

        db.session.add(user)
        db.session.commit()

        login_user(user, remember=True)

        return redirect(url_for('home'))


@app.route('/questionary', methods=['GET', 'POST'])
@login_required
def questionary():
    def process_field(data):
        return data if data != '' else None

    form = QuestionaryForm()

    if form.validate_on_submit():
        q = g.user.get_questionary()

        if q is None:
            q = Questionary(user_id=g.user.id)

        q.shoes_size = process_field(form.shoes_size.data)
        q.shirt_size = process_field(form.shirt_size.data)
        q.pants_size = process_field(form.pants_size.data)
        q.buying = process_field(form.buying.data)
        q.hobby = process_field(form.hobby.data)
        q.color = process_field(form.color.data)
        q.party = process_field(form.party.data)

        db.session.add(q)
        db.session.commit()

        # TODO set birthday
        return redirect(url_for('products'))

    else:
        flash(form.errors)

        form.birthday.data = g.user.birthday
        questionary = g.user.get_questionary()

        if questionary is not None:
            form.shoes_size.data = str(questionary.shoes_size)
            form.shirt_size.data = questionary.shirt_size
            form.pants_size.data = str(questionary.pants_size)
            if questionary.buying:
                form.buying.data = questionary.buying
            if questionary.hobby:
                form.hobby.data = questionary.hobby
            if questionary.color:
                form.color.data = questionary.color
            if questionary.party:
                form.party.data = questionary.party

        return render_template('form.html', form=form)


@app.route('/home')
@login_required
def home():
    if g.user.get_questionary() is None:
        return redirect(url_for('questionary'))

    else:
        return redirect(url_for('products'))

    ############
    #   like code
    ############
    # likes = facebook.get('me/likes').data['data']
    # likes_list = []
    # if likes:
    #     reqs = [{"method": "GET", "relative_url": x['id']} for x in likes]
    #     data = {'include_headers': False, 'batch': dumps(reqs)}
    #     post = facebook.post('/', data=data)

    #     if post.status == 200:
    #         for item in post.data:
    #             json_data = loads(item['body'])
    #             id = json_data['id']
    #             name = json_data['name']
    #             category = json_data['category']
    #             about = json_data['about']

    #             likes_list.append((id, name, category, about))
    ############


@app.route('/<nick>')
def user(nick):
    user = g.user if g.user.is_authenticated() else None
    prod = []

    if not g.user.is_authenticated() or nick != g.user.nickname:
        user = User.query.filter_by(nickname=nick).first()

    if user:
        join = Item.query.join(Opinion)
        prod = join.filter(Opinion.user_id == user.id, Opinion.liked == True)  # noqa

        return render_template('user.html', user=user, products=prod)

    else:
        return abort(404)


@app.route('/rename_party/', methods=['POST'])
@login_required
def rename_party():
    name = request.form.get('party-name', '').strip()

    if name:
        obj = Questionary.query.get(g.user.id)

        obj.party = name

        db.session.commit()

        return redirect(url_for('ajax_ok'))

    return abort(400)


@app.route('/products')
@login_required
def products():
    cnt = Opinion.query.filter(
        Opinion.user_id == g.user.id,  # noqa
        Opinion.liked == True,
        Opinion.item_id != None
    ).count()
    ctx = {'items': _get_items(), 'page': 1, 'count': min(cnt, 999)}

    return render_template('home.html', **ctx)


@app.route('/more_items/<page>/')
@login_required
def more_items(page):
    page = int(page) + 1

    if page > 1:
        html = ''

        for x in _get_items(page):
            html += render_template('card.html', card=x, page=page)

        return(html)

    return abort(400)


@app.route('/user_opinion/<itemid>/', methods=['POST'])
@login_required
def user_opinion(itemid):
    item = Item.query.get(itemid)
    like = loads(request.form['like']) if 'like' in request.form else None

    # TODO: If already exists, update.
    if item and isinstance(like, bool):
        obj = Opinion(user_id=g.user.id, item_id=itemid, liked=like)

        db.session.add(obj)
        db.session.commit()

        return redirect(url_for('ajax_ok'))

    return abort(400)


@app.route('/remove_opinion/<itemid>/', methods=['POST'])
@login_required
def remove_opinion(itemid):
    if Item.query.get(itemid):
        op = Opinion.query.filter_by(user_id=g.user.id, item_id=itemid).first()

        db.session.delete(op)
        db.session.commit()

        return redirect(url_for('ajax_ok'))

    return abort(400)


@app.route('/insert_item/', methods=['POST'])
@login_required
def insert_item():
    item = request.form.get('item', '')

    if item:
        obj = Opinion(user_id=g.user.id, item_name=item, liked=True)

        db.session.add(obj)
        db.session.commit()

        return redirect(url_for('ajax_ok'))

    return abort(400)


@app.route('/items_autocomplete/')
@login_required
def items_autocomplete():
    term = '%' + _SLUGY(request.args.get('term', '').strip()) + '%'
    lst = []

    if term:
        qry = Opinion.query
        qry = qry.filter(Opinion.user_id == g.user.id, Opinion.item_id != None)  # noqa
        qry = Item.query.filter(Item.id.notin_([x.item_id for x in qry]))

        for x in qry.filter(Item.normalized.like(term)).limit(_MAX):
            dic = {}

            dic['url'] = url_for('user_opinion', itemid=x.id)
            dic['name'] = x.name
            dic['price'] = x.price
            dic['image'] = x.image
            dic['description'] = x.description

            lst.append(dic)

        return Response(dumps(lst), content_type='application/json')

    return abort(400)


@app.route('/ajax_ok/')
@login_required
def ajax_ok():
    return Response('ok')


####################################
#      admin
####################################
@app.route('/admin', methods=['POST', 'GET'])
@login_required
def admin():
    if g.user.admin is True:
        form = EditItemForm()
        if form.validate_on_submit():
            flash('Valor atualizado')
            i = Item.query.get(form.id.data)
            i.name = form.name.data
            i.normalized = form.normalized.data
            i.category = form.category.data
            i.image = form.image.data
            i.price = form.price.data
            i.score = float(form.score.data)
            i.gender = form.gender.data
            i.reviewed = True
            i.store = form.store.data
            db.session.add(i)
            db.session.commit()
            return redirect(url_for('admin'))
        else:
            from sqlalchemy import or_
            for field, errors in form.errors.items():
                for error in errors:
                    flash(u"Error in the %s field - %s"
                          % (getattr(form, field).label.text, error))

            i = Item.query.filter(
                ~Item.url.like('%cultura%')
            ).filter(
                or_(Item.reviewed.is_(None), Item.reviewed == False)  # noqa
            ).order_by(func.random()).first()

            form.id.data = i.id
            form.name.data = i.name
            form.image.data = i.image
            form.normalized.data = i.normalized
            form.store.data = i.store
            form.category.data = i.category
            form.price.data = i.price

            total = Item.query.filter_by(
                reviewed=True).count()

            return render_template('form_edit_item.html', form=form, item=i,
                                   total=total)
    else:
        return redirect(url_for('logout'))
