# -*- coding: utf-8 -*-
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

####################################
#        facebook login
####################################
from flask_oauth import OAuth
from config import FACEBOOK_CONFIG

oauth = OAuth()
facebook = oauth.remote_app(
    'facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key=FACEBOOK_CONFIG['app_id'],
    consumer_secret=FACEBOOK_CONFIG['app_secret'],
    request_token_params={'scope': FACEBOOK_CONFIG['scope']}
)

####################################
#        login manager
####################################
from flask.ext.login import LoginManager

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

import views  # noqa : disable pep8 on this line
import models  # noqa : disable pep8 on this line
