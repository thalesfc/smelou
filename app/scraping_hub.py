# -*- coding: utf-8 -*-

from sys import argv, stderr
from json import loads
from app import db, models
from slugify import Slugify


slugy = Slugify(to_lower=True, separator='_')


if len(argv) != 2:
    print >> stderr, "Usage: <arquivo jl com itens>"
    exit(1)

path = argv[1]
print "# processando arquivo", path

data = [loads(l) for l in open(path)]
count = 0
for d in data:
    count += 1
    url = d['url']
    category = d.get('category', [None])[0]
    sku = d['sku'][0] if len(d['sku'][0]) < 32 else d['sku'][0][:32]
    description = d['description'][0]
    image = d['image'][0]
    price = ' '.join(d['price'][0].split()[0:2])
    price = price if len(price) < 16 else price[:16]
    name = d['name'][0]
    normalized = slugy(name)

#    print "description", description
#    print "sku", sku
#    print "url", url
#    print "category", category
#    print "image", image
#    print "price", price
#    print "name", name

    i = models.Item.query.filter_by(url=url).first()
    if i is None:
        i = models.Item(
            category=category, sku=sku, description=description,
            image=image, price=price, name=name, url=url,
            normalized=normalized)
    else:
        if i.reviewed is False:
            i.category = category
            i.price = price
            i.image = image
            i.sku = sku
            i.description = description
            i.name = name
            i.nomalized = normalized

    db.session.add(i)
    if count % 100 == 0:
        print count
        db.session.commit()
db.session.commit()
