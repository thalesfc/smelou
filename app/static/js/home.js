$(function() {
    $('#header-menu-icon').click(function() {
        $(this).toggleClass('green');
        $('#header-menu').toggleClass('display-none');

        return false;
    });

    $('html').click(function() {
        $('#header-menu-icon').removeClass('green');
        $('#header-menu').addClass('display-none');
    });

    $('.modal').click(function(e) {
        if (!$.contains(this, e.target)) {
            $(this).addClass('display-none');
        }
    });

    $('.explanation-tooltip .hide').click(function() {
        $(this).parent().addClass('display-none');
    });

    setInterval(function() {
        var curr = $('.bg.visible'),
            next = curr.hasClass('last') ? $('.bg.first') : curr.next();

        next.addClass('visible');
        curr.removeClass('visible');

    }, 6500);

    $('#form').submit(function() {
        if ($('#email-input').val().length) {
            if ($(this).find('button').is(':enabled')) {
                var $form = $(this),
                    request = null;

                $form.find('button').html('<div class="loading"></div>');
                $form.find('button').prop('disabled', true);
                $('#success-response').addClass('hidden');

                request = $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    data: $form.serialize()
                });

                request.fail(function() { $('#email-input').val(''); }).done(
                    function() {
                        $('#success-response').removeClass('hidden');
                        $form.find('button').html('Cadastrar');
                        $form.find('button').prop('disabled', false);
                        $('#email-input').val('');

                        setTimeout(function() {
                            $('#success-response').addClass('hidden');
                        }, 10000);
                    }
                );
            }
        }

        return false;
    });

    $('#card-view').on('click', '.opinion', function() {
        var $card = $(this).closest('.card'),
            doc = {'like': $(this).hasClass('like') ? true : false};

        if ($card.prevUntil().length == 3 || $card.prevUntil().length == 0) {
            $.get($card.data('get-more-url'), function(data, state, xhr) {
                $('#card-view').prepend(data);

            }).fail(function(data, state, xhr) {
                if (xhr == 'BAD REQUEST')
                    toastr.error('Requisição inválida.');

                else {
                    toastr.error('Erro de servidor.');
                }
            });
        }

        $.post($card.data('opinion-url'), doc).done(function(data, stat, xhr) {
            if (xhr.responseText == 'ok') {
                var total = parseInt($('#liked-counter span').text()) + 1;

                $card.remove();

                if (doc.like) {
                    $('#liked-counter span').text(total);
                }

            } else {
                toastr.error(data);
            }

        }).fail(function(data, state, xhr) {
            if (xhr == 'BAD REQUEST')
                toastr.error('Requisição inválida.');

            else {
                toastr.error('Erro de servidor.');
            }
        });
    });

    $('#inform-item input').focus(function() {
        $('#inform-item').addClass('expanded');
    });

    $('#inform-item').submit(function() {
        var $frm = $(this);

        $.post($frm.attr('action'), $frm.serialize()).done(function(d, s, x) {
            if (x.responseText == 'ok') {
                $frm[0].reset();

                $frm.removeClass('expanded');

                toastr.success('Produto adicionado.');

            } else {
                toastr.error(d);
            }

        }).fail(function(data, state, xhr) {
            if (xhr == 'BAD REQUEST')
                toastr.error('Requisição inválida.');

            else
                toastr.error('Erro de servidor.');
        });

        return false;
    });

    $('#rename-party').click(function() {
        $('#modal-rename-party').removeClass('display-none');
    });

    $('#modal-rename-party form').submit(function() {
        var $frm = $(this),
            path = $frm.attr('action'),
            name = $(this).find('input').val().trim();

        if (name.length) {
            $.post(path, $frm.serialize()).done(function(data, stat, xhr) {
                if (xhr.responseText == 'ok') {
                    $frm[0].reset();

                    $frm.parent().addClass('display-none');
                    $('#party-name').text(name);

                    toastr.success('Festa renomeada.');

                } else {
                    toastr.error(d);
                }

            }).fail(function(data, state, xhr) {
                if (xhr == 'BAD REQUEST')
                    toastr.error('Requisição inválida.');

                else
                    toastr.error('Erro de servidor.');
            });
        }

        return false;
    });

    $('#products-list .card .remove').click(function() {
        var $card = $(this).closest('.card');

        $.post($(this).data('url'), {}).done(function(data, stat, xhr) {
            if (xhr.responseText == 'ok') {
                var html = 0;

                $card.remove();

                html = '(' + $('#products-list .card').length;
                html += html == '(1' ? ' produto)' : ' produtos)';

                $('#products-counter').html(html);

            } else {
                toastr.error(data);
            }

        }).fail(function(data, state, xhr) {
            if (xhr == 'BAD REQUEST')
                toastr.error('Requisição inválida.');

            else {
                toastr.error('Erro de servidor.');
            }
        });
    });

    $('#order-by-price').click(function() {
        var $wrapper = $('#products-list'),
            $items = $wrapper.children();

        $items.sort(function(a,b) {
            var a0 = $(a).find('.item-price').text(),
                a1 = $(a).find('.item-name a').text(),
                b0 = $(b).find('.item-price').text(),
                b1 = $(b).find('.item-name a').text();

            a0 = parseFloat(a0.replace(/R\$ /g, '').replace(/\./g, ''));
            b0 = parseFloat(b0.replace(/R\$ /g, '').replace(/\./g, ''));

            if (a0 > b0) {
                return 1;

            } else if (a0 < b0) {
                return -1;

            } else if (a1 > b1) {
                return 1;

            } else if (a1 < b1) {
                return -1;

            } else {
                return 0;
            }
        });

        $items.detach().appendTo($wrapper);

    });

    // Jquery UI Autocomplete.
    //
    $('#autocomplete').each(function() {
        $('#autocomplete').autocomplete({
            source: $('#autocomplete').data('url'),
            appendTo: $('#inform-item'),
            minLength: 2
        });

        $(this).data('ui-autocomplete')._renderItem = function(ul, e) {
            var $a = $('<a/>'),
                $li = $('<li/>').data('item.autocomplete', e),
                $span = $('<span/>');

            $a.append($('<img/>').attr('src', e.image));

            $span.append($('<span class="name ellipsis"/>'));
            $span.append($('<span class="user-email ellipsis"/>'));
            $span.children().eq(0).text(e.name);
            $span.children().eq(1).text(e.email);
            $span.appendTo($a);

            return $li.html($a).addClass('ui-menu-item').appendTo(ul);
        };

        $(this).data('ui-autocomplete')._resizeMenu = function() {
            this.menu.element.outerWidth(this.element.outerWidth());
        };

        $(this).bind('autocompleteselect', function(event, ui) {
            if (ui.item && ui.item.url) {
                $.post(ui.item.url, {'like': true}).done(function(d, s, xhr) {
                    if (xhr.responseText == 'ok') {
                        $form[0].reset();

                        $form.removeClass('expanded');

                    } else {
                        toastr.error(d);
                    }

                }).fail(function(data, state, xhr) {
                    if (xhr == 'BAD REQUEST')
                        toastr.error('Requisição inválida.');

                    else {
                        toastr.error('Erro de servidor.');
                    }
                });
            }
        });
    });
});
