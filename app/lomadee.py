# -*- coding: utf-8 -*-

from sys import stderr
from json import loads
from urllib import urlencode
from socket import timeout, error
from urllib2 import urlopen, URLError
from urlparse import urlunparse

from app import db, models  # noqa
from models import Item
from sqlalchemy import or_

from time import sleep


_APP_ID = '6d53596b533534345a77773d'
_SOURCE_ID = '29114462'

BATCH_SIZE = 10


if __name__ == "__main__":
    items = Item.query\
        .filter(or_(Item.lomadee.is_(None), Item.lomadee is False))\
        .filter_by(store='sephora').all()

    cnt = 0

    for item in items:

        sleep(3)

        qry = []
        url = []
        tcp = None

        qry.append(('link1', item.url))
        qry.append(('format', 'json'))
        qry.append(('sourceId', _SOURCE_ID))

        url.append('http')
        url.append('bws.buscape.com')
        url.append('/service/createLinks/lomadee/%s/br' % _APP_ID)
        url.append('')
        url.append(urlencode(qry))
        url.append('')

        try:
            tcp = urlopen(urlunparse(url), timeout=10)
            dic = loads(tcp.read().decode('utf-8'))

            if dic.get('lomadeelinks', {}):
                url = dic['lomadeelinks'][0]['lomadeelink']['redirectlink']
                cnt = cnt + 1

                item.lomadee = url

                db.session.add(item)
                db.session.commit()

                print >> stderr, '%s\n%s\n\n\n' % (item.url, item.lomadee)

        except (URLError, timeout, error) as e:
            print >> stderr, '$ Item url: %s\n$ Lomadee url: %s' \
                % (item.url, url)
            print >> stderr, '---------\n%s\n---------\n' % (e)

        finally:
            if tcp:
                tcp.close()

        # if cnt == BATCH_SIZE:
        #    cnt = 0
        #    db.session.commit()
    # db.session.commit()
