# -*- coding: utf-8 -*-

from app import db


USER_ID_SIZE = 32


class Questionary(db.Model):
    user_id = db.Column(
        db.String(USER_ID_SIZE),
        db.ForeignKey('user.id'),
        primary_key=True)
    shoes_size = db.Column(db.SmallInteger)
    shirt_size = db.Column(db.String(2))
    pants_size = db.Column(db.SmallInteger)
    buying = db.Column(db.String(64))
    hobby = db.Column(db.String(64))
    color = db.Column(db.String(64))
    party = db.Column(db.String, default=None)


class Opinion(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    user_id = db.Column(
        db.String(USER_ID_SIZE),
        db.ForeignKey('user.id'),
        index=True)
    item_id = db.Column(
        db.Integer,
        db.ForeignKey('item.id'),
        index=True,
        nullable=True)
    item_name = db.Column(db.Text)
    liked = db.Column(db.Boolean, index=True)


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(128), index=True)
    sku = db.Column(db.String(32), index=True)
    description = db.Column(db.Text)
    image = db.Column(db.String(512))
    price = db.Column(db.String(16))
    name = db.Column(db.String(128))
    normalized = db.Column(db.String(128), index=True)
    url = db.Column(db.String(512), index=True, unique=True)
    score = db.Column(db.Float, default=0.0)
    gender = db.Column(db.String(1), default='B')
    reviewed = db.Column(db.Boolean, default=False)
    store = db.Column(db.String(32))
    lomadee = db.Column(db.String(512))

    def __repr__(self):
        return '<Item (%r) %r>' % (self.id, self.name)


class User(db.Model):
    id = db.Column(db.String(USER_ID_SIZE), primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(128), index=True, unique=True)
    picture = db.Column(db.String(512))
    birthday = db.Column(db.Date())
    name = db.Column(db.String(128), index=True)
    gender = db.Column(db.String(16))
    first_name = db.Column(db.String(64))
    fb_link = db.Column(db.String(512))
    admin = db.Column(db.Boolean, default=False)

    def get_questionary(self):
        return Questionary.query.get(self.id)

    @staticmethod
    def generate_nickname(full_name):
        nick = full_name.replace(' ', '.').lower()
        if User.query.filter_by(nickname=nick).first() is None:
            return nick
        version = 1
        while True:
            new_nickname = nick + str(version)
            if User.query.filter_by(nickname=new_nickname).first() is None:
                break
            version += 1
        return new_nickname

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User (%r) %r>' % (self.id, self.nickname)
