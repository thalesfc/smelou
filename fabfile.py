# -*- coding: utf-8; -*-

from fabric.api import local, run, env, put, settings, sudo

env.hosts = ['www.smelou.com']
env.user = 'admin'
env.key_filename = '~/.ssh/id_rsa'

_LOCAL_DIR = 'app'
_ARCHIVE_NAME = 'release'
_SERVER_DIR = '/var/www/smelou/smelou'


def upload_archive():
    print('creating archive…')

    local('cd %s && zip -qr /tmp/%s.zip -x=*.pyc -x=config.py *'
          % (_LOCAL_DIR, _ARCHIVE_NAME))

    print('uploading archive…')

    put('/tmp/%s.zip' % _ARCHIVE_NAME, '/tmp/')

    print('extracting code…')

    run('cd /tmp && mkdir prev_files && unzip -q %s.zip -d ./%s'
        % (_ARCHIVE_NAME, _ARCHIVE_NAME))


def update_files():
    var = _SERVER_DIR

    print('backup previous files…')

    run('touch %s/foo' % var)
    run('mv %s/* /tmp/prev_files/' % var)

    print('setup new files…')

    run('mv /tmp/%s/* %s/' % (_ARCHIVE_NAME, var))
    run('cp /tmp/prev_files/config.py %s/' % var)

    with settings(shell='/bin/bash -c'):
        sudo('chown -R admin %s/*' % var, pty=False)
        sudo('chgrp -R www-data %s/*' % var, pty=False)
        sudo('find %s -not -type d -exec chmod 0655 {} \;' % var, pty=False)
        sudo('find %s -type d -exec chmod 0775 {} \;' % var, pty=False)


def restart():
    print('restart services…')

    with settings(shell='/bin/bash -c'):
        sudo('service nginx restart', pty=False)
        sudo('service uwsgi restart', pty=False)


def cleanup():
    print('cleanup…')

    run('rm -f /tmp/%s.zip' % _ARCHIVE_NAME)
    run('rm -rf /tmp/%s' % _ARCHIVE_NAME)
    run('rm -rf /tmp/prev_files')
    local('rm -f /tmp/%s.zip' % _ARCHIVE_NAME)


def deploy():
    cleanup()
    upload_archive()
    update_files()
    restart()
    cleanup()

    print('deploy complete!')
