// Handle request state change events
smelou_submited = function() {
    // If the request completed
    if (this.readyState == 4) {
        if (this.status == 202) {
            // If it was a success, close the popup after a short delay
            alert('Seu perfil do smelou ja esta completo. \n Acesse www.smelou.com e descubra como eh facil receber bons presents :)');

        } else if (this.status == 200) { // normal incremental execution
            console.log('Perfil smelou atualizado');

        } else if (this.status == 401) {
            alert('Ops, voce deve logar no smelou para poder submeter seus dados');

        } else if (this.status == 403) {
            alert('Erro: você está tentando enviar os dados para conta de outro usuário.\n Por favor, entre em contato com o responsável pelo portal');

        } else {
            // Show what went wrong
            alert('# ERROR CODE: ' + this.status + '\nErro ao enviar seus dados para o smelou.\nPor favor, entre em contato com o responsável pelo portal');
        }
    }
};

function smelou() {
    // config
    var stime = new Date(),
        userid = null; // user logging

    stime.setFullYear(stime.getFullYear() - 3);

    chrome.cookies.get({
            'url': 'http://localhost',
            'name': 'smelou_user_id'
        },

        function(cookie) {
            if (cookie == null) {
                alert('Por favor, faça login no www.smelou.com e depois reinicie o seu navegador.');
                return;
            }

            userid = cookie.value;

            // history analytics
            chrome.history.search({
                'text': '',
                'startTime': stime.getTime(),
                'endTime' : new Date().getTime(),
                'maxResults': 0
            },

            function(history_items) {
                var xhr = new XMLHttpRequest();

                console.log('Smelou! ' + history_items.length + ' -> user:' + userid);

                // transfering information
                xhr.onreadystatechange = smelou_submited;

                // TODO change to smelou.com
                xhr.open('POST', 'http://localhost:5000/submit/' + userid, true);
                xhr.setRequestHeader('Content-type', 'application/json');
                xhr.send(JSON.stringify(history_items));
            });
        });
}

/***
 * starting
 */
var oldChromeVersion = !chrome.runtime;

if (oldChromeVersion) {
    onInit();

} else {
    chrome.runtime.onInstalled.addListener(smelou);
}
